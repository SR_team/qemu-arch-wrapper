import std.process : spawnProcess, wait;
import std.path : baseName;

int main(string[] args)
{
	auto arch = baseName(args[0]);
	return wait(spawnProcess(["/usr/bin/qemu-"~arch, "-L", "/usr/"~arch~"-linux-gnu/", "-E",
			"LD_LIBRARY_PATH=/usr/"~arch~"-linux-gnu/lib64/:/usr/"~arch~"-linux-gnu/lib/"] ~ args[1..$]));
}
